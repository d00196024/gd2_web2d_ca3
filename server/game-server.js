/**
 * Starts a HTTP server with Express to route REST API requests/responses
 * @author niall mcguinness 
 */
const dotenv = require('dotenv');
dotenv.config();
const express = require('express');
const http = require('http');

const router = express();
router.use(express.json());

const playerRoute = require('./routes/player-data-routes');
router.use('/', playerRoute);

const server = http.createServer(router);
const db = require('./controllers/db');

db.connect(process.env.DB_URL, process.env.DB_NAME, (err) => {
    if (err) {
        console.log('unable to connect to database');
        process.exit();
    } else {
        server.listen(process.env.HTTP_SERVER_PORT, "127.0.0.1", function () {
            console.log('connected and listening on port ' + process.env.HTTP_SERVER_PORT);
        });
    }
});