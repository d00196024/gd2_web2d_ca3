/**
 * Represents the data to be stored in the MongoGO
 * @author niall mcguinness 
 */

const mongoose = require('mongoose');

//Mongoose ODM schema
const PlayerDataSchema = new mongoose.Schema({ 
    game: {  //game name
        type: String, 
        required: true,
        trim: true
    },
    age: {
        type: Number,
        required: false,
        min: 18,
        max: 100,
        default: 18
    },
    player: {  //player name
        type: String, 
        required: true,
        trim: true
    },
    score:{ //score!
        type: Number,
        required: true,
        min: -1000000,
        max:  1000000
    }, //entry date
    creationdate:{
        type: Date,
        default: Date.now()
    }

});

module.exports = mongoose.model('PlayerData', PlayerDataSchema);
