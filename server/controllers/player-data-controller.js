/**
 * Provides CRUD operations for the player data
 * @author niall mcguinness 
 */

const db = require('./db');
const PlayerData = require('../models/player-data');

/**
 * Creates a record in the collection based on the JSON object using POST
 * 
 * @command POST
 * @url http://127.0.0.1:3000/create
 * @example we send the JSON object below either in our code using GDREST 
 * {
 *      game: "split screen sam",
 *      player: "jane doe",
 *      score: 324
 * }
 * or we send the JSON object in POSTMAN but with "" around the key variables
 * {
 *      "game": "split screen sam",
 *      "player": "jane doe",
 *      "score": 324
 * }
 * 
 * @returns Array of JSON objects or error
 */
exports.createOne = function (req, res) {
    let playerData = new PlayerData({
        game: req.body.game,
        player: req.body.player,
        age: req.body.age,
        score: req.body.score
    });

    db.getDatabase().collection(process.env.DB_COLLECTION_NAME).insertOne(playerData, (err, result) => {
        if (err) {
            return res.status(503).json({
                type: 'Database Error: Failed to create a record',
                message: err.message
            });
        } else {
            res.header("Access-Control-Allow-Origin", "*");
            res.json(result);
        }
    });
}

/**
 * Reads first JSON object in the collection with the ID specified in request URL path
 * 
 * @command GET
 * @url http://127.0.0.1:3000/readone
 * @example we send the JSON object below either in our code using GDREST 
 * {
 *      id: "5ea80a60cc100d1a4ad5634c"
 * }
 * or we send the JSON object in POSTMAN but with "" around the key variables
 * {
 *      "id": "5ea80a60cc100d1a4ad5634c"
 * }
 * and we receive the JSON object below
 * {
 *      game: "split screen sam",
 *      player: "jane doe",
 *      score: 324
 * }
 * 
 * @returns Array of JSON objects or error
 */
exports.readOne = function (req, res) {
    db.getDatabase().collection(process.env.DB_COLLECTION_NAME).find({
        _id: db.getPrimaryKey(req.body.id)
    }).toArray((err, documents) => {
        if (err) {
            return res.status(503).json({
                type: 'Database Error: Failed to read a record',
                message: err.message
            });
        } else {
            res.header("Access-Control-Allow-Origin", "*");
            res.json(documents);
        }
    });
}

/**
 * Reads and returns all the collection as an array of JSON objects
 * 
 * @command GET
 * @url http://127.0.0.1:3000/readall
 * @example we receive the array of JSON objects below
 * [{
 *      game: "split screen sam",
 *      player: "jane doe",
 *      score: 234
 * },
 * {
 *      game: "split screen sam",
 *      player: "bob doe",
 *      score: 653
 * },
 * {
 *      game: "split screen sam",
 *      player: "jim doe",
 *      score: 999
 * }]
 *
 * @returns Array of JSON objects or error
 */
exports.readAll = function (req, res) {
    db.getDatabase().collection(process.env.DB_COLLECTION_NAME).find({}).toArray((err, documents) => {
        if (err) {
            return res.status(503).json({
                type: 'Database Error: Failed to read records',
                message: err.message
            });
        } else {
            res.header("Access-Control-Allow-Origin", "*");
            res.json(documents);
        }
    });
}


/**
 * Updates and returns the first JSON object in the collection matching the ID specified in the JSON object provided
 *
 * @command PUT
 * @url http://127.0.0.1:3000/update
 * @example we send the JSON object below either in our code using GDREST 
 * {
 *      id: "5ea80a60cc100d1a4ad5634c", //id
 *      game: "new split screen sam",   //updated
 *      player: "jane",
 *      score: 123
 * }
 * or we send the JSON object in POSTMAN but with "" around the key variables
 * {
 *      "id": "5ea80a60cc100d1a4ad5634c", //id
 *      "game": "new split screen sam",   //updated
 *      "player": "jane",
 *      "score": 123
 * }
 * and we receive the newly updated JSON object below
 * {
 *      game: "new split screen sam",
 *      player: "jane new name",
 *      score: 123
 * }
 * @returns JSON object or error
 */
exports.updateOne = function (req, res) {
    const id = req.params.id;
    db.getDB().collection(process.env.DB_COLLECTION).findOneAndUpdate({
        _id: db.getPrimaryKey(id)
    }, {
        $set: {
            name: req.body.name,
            section: req.body.section,
            price: req.body.price,
            vegetarian: req.body.vegetarian,
            createdby: req.body.createdby
        }
    }, {
        returnOriginal: false
    }, (err, result) => {
        if (err) {
            //tested error code but it does not get called for some reason
            /*
            const error = new Error("Failed to update a record");
            error.status = 503;
            next(error);
            */
            return res.status(503).json({
                type: 'Database Error: Failed to update a record',
                message: err.message
            });
        } else {
            //had some CORS errors from POSTMAN and browser early in the development and added this
            res.header("Access-Control-Allow-Origin", "*");
            res.json(result);
        }
    });
}

/**
 * Deletes and returns the (newly deleted) first JSON object in the collection matching the ID specified in request URL path
 * 
 * @command DELETE
 * @url http://127.0.0.1:3000/delete
 * @example we send the JSON object below either in our code using GDREST 
 * {
 *      id: "5ea80a60cc100d1a4ad5634c"
 * }
 * or we send the JSON object in POSTMAN but with "" around the key variables
 * {
 *      "id": "5ea80a60cc100d1a4ad5634c"
 * }
 * 
 * @returns JSON object or error
 */
exports.deleteOne = function (req, res) {
    const id = req.body.id;
    db.getDatabase().collection(process.env.DB_COLLECTION_NAME).findOneAndDelete({
        _id: db.getPrimaryKey(req.body.id)
    }, (err, result) => {
        if (err) {
            return res.status(503).json({
                type: 'Database Error: Failed to delete a record',
                message: err.message
            });
        } else {
            res.header("Access-Control-Allow-Origin", "*");
            res.json(result);
        }
    });
}

/**
 * Deletes multiple rows in the collection matching the IDs specified in request body
 *  
 * @command DELETE
 * @url http://127.0.0.1:3000/deletemany
 * @example we send the JSON object below either in our code using GDREST 
 * {
 *      id: "5ea80a60cc100d1a34fe634c",
 *      id: "5ea80a60cc100d1a4ad56256",
 *      id: "5ea80a60cc100d1a4a5342ef"
 * }
 * or we send the JSON object in POSTMAN but with "" around the key variables
 * {
 *      "id":" "5ea80a60cc100d1a34fe634c",
 *      "id": "5ea80a60cc100d1a4ad56256",
 *      "id": "5ea80a60cc100d1a4a5342ef"
 * }
 * 
 * @returns JSON object or error
 */
exports.deleteMany = function (req, res) {

    let ids = req.body.map(id => db.getPrimaryKey(id));
    db.getDatabase().collection(process.env.DB_COLLECTION_NAME).deleteMany({
        _id: {
            $in: ids
        }
    }, (err, result) => {
        if (err) {
            return res.status(503).json({
                type: 'Database Error: Failed to delete a record',
                message: err.message
            });
        } else {
            res.header("Access-Control-Allow-Origin", "*");
            res.json(result);
        }
    });
}