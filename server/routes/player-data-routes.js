/**
 * Routes requests to the controller specified
 * @author niall mcguinness 
 */
const express = require('express');
const router = express.Router();
const controller = require('../controllers/player-data-controller');

router.post('/create', controller.createOne);
router.get('/readone', controller.readOne);
router.get('/readall/', controller.readAll);

//to do...test methods with postman
router.put('/update', controller.updateOne);  
router.delete('/delete', controller.deleteOne);  
router.delete('/delete/many', controller.deleteMany);  


/********************************************* A NON-DB RELATED EXAMPLE *********************************************/
//NodeJS & Express dont always have to connect to DBs they can also execute a function and (possibly) return a value

/**
 * Adds two numbers
 *  
 * @command GET
 * @path http://127.0.0.1:3000/maths/engine/add
 * @example we send the JSON object below either in our code using GDREST 
 * {
 *      x: 45,
 *      y: 50
 * }
 * or we send the JSON object in POSTMAN but with "" around the key variables
 * {
 *      "x": 45,
 *      "y": 50
 * }
 * 
 * @returns JSON object or error
 * @author niall mcguinness
 */

router.get('/maths/engine/add', function (req, res) {

    //something simple...
    let x = req.body.x;
    let y = req.body.y;

    //lets imagine that the function can throw an exception
    try{
        //do the work
        let total = x + y;

        //allow connections from code not running on the same server as our service
        res.header("Access-Control-Allow-Origin", "*");

        //return the JSON object
        res.json({
            operation: "add", 
            result: total});
    
    }
    //catch the "bad thing that happens" and send a fail message
    catch(err) {
        //set the status and return the JSON error object
        return res.status(503).json({
            type: 'Maths Engine Error: Failed to add the values!',
            message: err.message
        });
    }
});


module.exports = router;