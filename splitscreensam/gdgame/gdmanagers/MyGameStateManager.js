/**
 * This class is responsible for listening, and responding to, notifications 
 * relating to game state changes (e.g. win/lose, fire bullet, play animation over pickup)
 * @author niall mcguinness
 * @version 1.0
 * @class MyGameStateManager
 */

class MyGameStateManager extends GameStateManager {
    //#region  Fields 
    //#endregion 
    p1Score = 0;
    p2Score = 0;
    p1Health = 0;
    p2Health = 0;
    //#region  Properties
    //#endregion

    constructor(id, statusType, objectManager, cameraManager, 
        notificationCenter, spriteArchetypeArray, screenObjectArray) {
        super(id);
        this.id = id;
        this.statusType = statusType;
        this.objectManager = objectManager;
        this.cameraManager = cameraManager;

        //an array that stores the archetypes to be used by the game state manager
        this.spriteArchetypeArray = spriteArchetypeArray;

        //an array of the objects that contain information that this class needs to process and show UI notifications (e.g. playerID, uiID) 
        //for example, when a notification is received which of the two game screens (and player states) does it affect?
        this.screenObjectArray = screenObjectArray;

        //listen for notifications including menu related to set the statustype of this manager appropriately (e.g. as with ObjectManager, RenderManager)
        this.notificationCenter = notificationCenter;
        this.RegisterForNotifications();

        //var score = 0;

        //setup the UIs for the players listed in screenObjectArray
        this.InitializeAllUIs();
    }

    /**
     * Setup all the UIs for the players in the game
     *
     * @memberof MyGameStateManager
     */
    InitializeAllUIs() {
        for (let screenObject of this.screenObjectArray)
            this.InitializeDemoUI(screenObject.uiID);
    }


    /**
     * Creates a demo UI for each player in the screenObjectArray and adds the HTML elements (e.g. div_score) to the correct canvas
     * You should change this method to add whatever UI elements your game uses.
     * @param {*} uiID
     * @memberof MyGameStateManager
     */
    InitializeDemoUI(uiID) {

        //some value to start our score with
        //let score = document.getElementById("Something");
        let initialHealth = 100;
        
        let initialScore = 0;
        //this.p1Health = initialHealth;
        //this.p2Health = initialHealth;
        //this.p1Score = initialScore;
        //this.p2Score = initialScore;
        //initialScore.id = "init-ui-score";
        //let score.id = "score_ui";
        //score = initialScore;
        //get a handle to the div that we use for adding UI elements for this particular canvas (e.g. player-ui-top)
        let container = document.querySelector("#" + uiID);

        //create element to store the score
        let div_score = document.createElement("div");
        div_score.id = "score-ui";
        //set text
        div_score.innerText = "Score: " + initialScore;
        //now class for any effects e.g. fadein
        div_score.setAttribute("class", "ui-info");
        //now position
        div_score.setAttribute("style", "bottom: 1%; right: 1%;");
        //add to the container so that we actually see it on screen!



        let div_health = document.createElement("div");
        div_health.id = "health-ui";
        div_health.innerText = "Health: " + initialHealth;
        div_health.setAttribute("class", "ui-info");
        div_health.setAttribute("style", "bottom: 1%; right: 87%;");


        container.appendChild(div_health);
        container.appendChild(div_score);
    }

    UpdateGameWinLose(player, score)
    {
        console.log(player+" Wins! With"+ score + " points");

        //MyMenuManager.FinishGame(player, score);

        //this.notificationCenter.Register(
        //    NotificationType.GameState,
        //    NotificationAction.Win,
        //
        //);

        //NotificationCenter.Notify(
        //    new Notification(
        //      NotificationType.Menu, 
         //     NotificationAction.Win,  
        //     [
         //       player,
         //       score
        //      ])); 

              


    }

    UpdateScore(scoreIn)
    {

        //console.log(scoreIn);
        //let score = parseInt(document.getElementById("ui-info").innerText);
        //console.log(score);
        //let currScore = parseInt(score.innerText);

        //score.innerText = scoreIn+currScore;
    }


    //#region Notification Handling
    RegisterForNotifications() {

        //register for menu notification
        this.notificationCenter.Register(
            NotificationType.Menu,
            this,
            this.HandleNotification
        );

        //register for pickup notifications so we can play animations, change game state (e.g. win/lose or add points to player UI)
        this.notificationCenter.Register(
            NotificationType.GameState,
            this,
            this.HandleNotification
        );
    }

    HandleNotification(...argArray) {
        let notification = argArray[0];
        switch (notification.NotificationAction) {
            case NotificationAction.ShowMenuChanged:
                this.HandleShowMenuChanged(notification.NotificationArguments[0]);
                break;

            case NotificationAction.Pickup:
                this.HandlePickup(
                    notification.NotificationArguments[0], //value of the pickup (e.g. 5 points)
                    notification.NotificationArguments[1], //id of the animation decorator to play (e.g. "coin_pickup_decorator") which must be already stored by MyGameStateManager::spriteArchetypeArray
                    notification.NotificationArguments[2], //reference to player that picked it up so we can obtain player ID and know which canvas to update
                    notification.NotificationArguments[3], //reference to the pickup sprite so that we can obtain its position and draw any animated decorator at that point
                    notification.NotificationArguments[4]); //string to show when the pickup is collected
                break;

            //case NotificationAction.UpdateScore:
            //    this.UpdateScore{
            //        notification.NotificationArguments[0]
            //    }

            default:
                break;
        }
    }

    HandleShowMenuChanged(statusType) {

        //set the status of the manager so that its Update() will execute (or not if StatusType.Off)
        this.statusType = statusType; //statusType to set this manager to (e.g. Off, IsDrawn, IsDrawn | IsUpdated)

        //set the UI to visible or hidden
        this.ToggleDisplayUIs(statusType);
    }

    ToggleDisplayUIs(statusType) {
        //loop through each of the two players and get the uiID of the DIV for that player
        for (let screenObject of this.screenObjectArray) {
            //if show then set display to none (i.e. hidden)
            if (statusType == StatusType.Off)           
                document.querySelector("#" + screenObject.uiID).setAttribute("style", "display: none;");
            //if show then set display to block (i.e. visible)
            else                                       
                document.querySelector("#" + screenObject.uiID).setAttribute("style", "display: block;");
        }
    }

    /**
     * Called when a behavior (e.g. SamPlayerBehavior) publishes a notification with NotificationType.GameState and NotificationAction.Pickup
     *
     * @param {number} value Value of the pickup (e.g. 5 for ammo)
     * @param {string} decoratorID ID (found in spriteArchetypeArray) of the decorator to show when this pickup is collided with. 
     * @param {Sprite} parent Reference to the sprite (i.e. the player object) that collected the pickup
     * @param {Sprite} sprite Reference to the sprite that was collected
     * @memberof MyGameStateManager
     */
    HandlePickup(value, decoratorID, parent, sprite, description) {
        //show an animation (based on archetype provided to InitializeArchetypes())
        this.ShowPickupAnimatedDecorator(decoratorID, sprite, description);
        //console.log("pickup value: "+ value);
        //update the UI
        //let parent = parentIn
        //console.log("HandlePickup: "+parent.id);
        if(parent.id == "player1")
        {
            //console.log("HandlePickup - player1");
            let parentUI1 = "#" + this.screenObjectArray[0].uiID;
            this.UpdateGameState("player1", value, parentUI1, sprite);
            //this.UpdateUIScore(parent, value, ui, sprite);
        }
        else{
            //console.log("HandlePickup - player2");
            let parentUI2 = "#" + this.screenObjectArray[1].uiID;
            this.UpdateGameState("player2", value, parentUI2, sprite);
            //this.UpdateUIScore(parent, value, ui, sprite);
        }
        //this.UpdateUI(value, parent, sprite);
        this.Update();
        //update the game state
        //this.UpdateGameState(value, parent, sprite);
    }

    //#endregion


    /**
     * Called to add an animated decorator for a pickup to the object manager
     *
     * @param {number} value Value of the pickup (e.g. 5 for ammo)
     * @param {string} decoratorID ID (found in spriteArchetypeArray) of the decorator to show when this pickup is collided with. 
     * @param {Sprite} sprite Reference to the sprite that was collected so that we can obtain its translation
     * @memberof MyGameStateManager
     */
    ShowPickupAnimatedDecorator(decoratorID, sprite, description) {
     
        let bUseCSS = true;

        //we can use our spritesheet
        if(!bUseCSS)
        {
            //clone the decorator from the archetypeArray
            let decorator = this.spriteArchetypeArray[decoratorID].Clone();
            //turn the decorator (i.e. the animated sprite on)
            decorator.statusType = StatusType.IsDrawn | StatusType.IsUpdated;
            //set the position of the sprite
            decorator.Transform2D.SetTranslation(Vector2.Subtract(sprite.transform2D.translation, sprite.transform2D.origin));
            //add to the object manager
            this.objectManager.Add(decorator);            
        }
//#region DEMO ONLY - ONLY DRAWS TO TOP SCREEN AND HAS BUG WHEN CAMERA MOVES
         //or we can use CSS instead
        else{
            //this is a rough approximation of the position based on the players position 
            let x = sprite.transform2D.translation.x;
            let y = sprite.transform2D.translation.y;

            //notice that I'm just sending this notification to the top screen 
            let container = document.querySelector("#" + this.screenObjectArray[0].uiID);
            //create element to store the score
            let div_score = document.createElement("div");
            //set text
            div_score.innerText = description;
            //now class for any effects e.g. fadein
            div_score.setAttribute("class", "ui-info-pickup-value");
            //now position
            div_score.setAttribute("style", "left:" + x + "px; top:" + y + "px;");
            //add to the container so that we actually see it on screen!
            container.appendChild(div_score);
        }
//#endregion

    }

    UpdateGameState(player, value, parent, sprite) 
    {
        //console.log("update game state");
        //update scores?
        if(player == "player1")
        {
            //console.log("player 1: "+value);
            this.p1Score+=value;
            this.UpdateUIScore(player, parent);
        }
        else{
            //console.log("player 2: "+value);
            this.p2Score+=value;
            this.UpdateUIScore(player, parent);
        }
        //this.UpdateUIScore(player, this.screenObjectArray.getElementById("player2"));
        //check score for win?
        //this.CheckCondition();
        
    }

    UpdateUIScore(player, parent) 
    {

        //console.log("UpdateUIScore "+player);
        //console.log("p1 = "+this.p1Score);
        //console.log("p2 = "+this.p2Score);

        if(player=="player1")
        {
            //console.log("p1");
            let score = this.p1Score;
            //let id = "#"+ui.uiID
            this.UpdateUI(parent, score);
        }
        if(player=="player2")
        {
            //console.log("p2");
            let score = this.p2Score;
            //let id = "#"+ui.uiID
            this.UpdateUI(parent, score);
        }
    }

    UpdateUI(id, score)
    {
        //console.log("draw ui");

        let container = document.querySelector(id);

        let scoreUI = document.getElementById("score-ui");
        //set text

        scoreUI.innerText = "Score: " + score;
        //now class for any effects e.g. fadein
        scoreUI.setAttribute("class", "ui-info");
        //now position
        scoreUI.setAttribute("style", "bottom: 1%; right: 1%;");
        //add to the container so that we actually see it on screen!

        container.appendChild(scoreUI);
    }

    Update(gameTime) {

        //if(this.p1Score=100)
        //{
        //    this.p1Health+=20
        //}
        //if(this.p2Score=100)
        //{
        //    this.p2Health+=20
        //}
        // breaks score counter???

        if(this.p1Score>100)//100 for quick demo
        {
            this.UpdateGameWinLose("Player 1", this.p1Score);
        }
        if(this.p2Score>100)
        {
            this.UpdateGameWinLose("Player 2", this.p2Score);
        }
    }

    //#region Equals, Clone, ToString 
    Equals(other) {
        //to do...  
        throw "Not Yet Implemented";
    }

    ToString() {
        //to do...
        throw "Not Yet Implemented";
    }

    Clone() {
        //to do...
        throw "Not Yet Implemented";

    }
    //#endregion
}