﻿## Codebase - L8 - Y2 - Game Development ##

### Instructions for adding MongoDB functionality & Leaderboard (CA3 Only) ###

1. **Install** [NodeJS](https://nodejs.org/en/download/) on your machine by following any [online tutorial](https://phoenixnap.com/kb/install-node-js-npm-on-windows)

2. **Run** the commands by opening a terminal
~~~~~
npm install mongodb mongoose http express dotenv --save
node server/game-server   
~~~~~

3. **Read** the documentation above each method in player-data-controller.js to know how to use the REST API operation (e.g. createOne)

4. **Install** and use [POSTMAN](https://www.postman.com/downloads/) to test the REST API endpoints (i.e. the URLS that do the CRUD operations listed in player-data-routes.js)

5. **Ensure** that you stop your server when you finish by clicking on the terminal window and pressing CTRL+C

6. **Create** a [MongoDB Cloud](https://www.mongodb.com/cloud/atlas/signup) account and then create a database and collection as shown in the screencast in Moodle for 28th April 2020 and replace your details with mine in the .env file

7. **Experiment** with NodeJS and Express by adding your own REST endpoints in player-data-routes.js and player-data-controller.js. Start by looking at the add() REST endpoint in player-data-routes.js. You don't have to 
separate the route definition (e.g. router.post('/create', controller.createOne)) and the handler function (e.g. createOne()) into two separate files - we just do it for scalability and maintainability.


### Screencasts ###

**Vector2**
* https://www.youtube.com/watch?v=oOD3HclyHA0

**Rect**
* https://www.youtube.com/watch?v=hW6QuHy48Qs

**Circle**
* https://www.youtube.com/watch?v=aCZa0Fz6gu0

**Matrix**
* https://www.youtube.com/watch?v=MzIW8627RJU

**Transform2D**
* https://www.youtube.com/watch?v=VntYch4Kx1s

**Actor2D**
* https://www.youtube.com/watch?v=Jf-zcRfAjm8

**Camera2D**
* https://www.youtube.com/watch?v=eQzDXdLgD8Y

**Sprite & MoveableSprite**
* https://www.youtube.com/watch?v=so5zOwD-38M

**Body**
* https://www.youtube.com/watch?v=UryLdF0INmw

**Collision**
* https://www.youtube.com/watch?v=zwRGQWzkC-s
* https://www.youtube.com/watch?v=3kIZWH-ehyA
* https://www.youtube.com/watch?v=A0KYrc9VLdg

**MyMenuManager & MyStateManager**
* https://2020-moodle.dkit.ie/pluginfile.php/232950/mod_resource/content/1/2DGD%20-%20Performance%20MyMenuManager%20and%20GameStateManager.mp4

**To Do**
* NotificationCenter
* MyConstants
* SamPlayerBehavior





